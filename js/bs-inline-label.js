/*!
  * Bootstrap 4 Input Inline-Label
  * Copyright Stefan Piffl (https://www.drpiffl.de)
  * Licensed under MIT (https://gitlab.com/DrPiffl/bs-modal-load/blob/master/LICENSE)
  */
( function( $ ) {
    $.fn.inlineLabel = function() {
        $.each( this ,function(i,element) {
            let $this = $(element);
            let $formControl = $('.form-control', $this);
            let $label = $('label', $this);

            // switch placeholder
            $formControl.attr('ph', $formControl.attr('placeholder')).attr('placeholder', '');

            // move label if formControl has value
            if (typeof $formControl.val() === 'string' && $formControl.val().trim() !== '') {
                $this.addClass('moved');
            }

            // trigger formControl focus
            $label.click(function () {
                $formControl.focus();
            });

            // move label and show placeholder
            $formControl.focus(function () {
                $this.addClass('moved');
                $formControl.attr('placeholder', $formControl.attr('ph'));
            });

            // move label and hide placeholder
            $formControl.focusout(function () {
                $formControl.attr('placeholder', '');
                if (typeof $formControl.val() === 'string' && $formControl.val().trim() === '') {
                    $this.removeClass('moved');
                }
            });
        } );
    };

    $( function() {
        // register on DOM content loaded
        $( '.inline-label' ).inlineLabel();

        // register on DOM node inserted
        $( document ).on( 'DOMNodeInserted', '* > .inline-label', function() {
            $( this ).inlineLabel();
        } );
    } );
}( jQuery ) );